<?php

//Remove Enqueue Scripts
remove_action( 'wp_enqueue_scripts', 'required_load_scripts' );

// Thumbnails Support
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
}

function get_version_number()
{
	static $assetsVersion;
	if (empty($assetsVersion)) {
		$assetsVersion = file_get_contents(get_template_directory() . '/assets.version');
	}
	return $assetsVersion;
}

// Change Footer Text
add_filter( 'admin_footer_text', 'my_footer_text' );
add_filter( 'update_footer', 'my_footer_version', 11 );
function my_footer_text() {
	return '<i>Gamers Unite</i>';
}
function my_footer_version() {
	return 'Version 1.0';
}

/* Directory Tree */
define( 'JSPATH', get_template_directory_uri() . '/assets/scripts/' );
define( 'CSSPATH', get_template_directory_uri() . '/assets/css/' );
define( 'THEMEPATH', get_template_directory_uri() . '/' );
define( 'IMGPATH', get_template_directory_uri() . '/assets/img/' );
define( 'SITEURL', site_url('/') );

/* Enqueue scripts and styles. */
function scripts() {
	// Load CSS
	wp_enqueue_style( 'fontMaterialDesign', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
	wp_enqueue_style( 'styles', CSSPATH . 'app.css', array(), get_version_number() );
	// Load JS
	wp_enqueue_script('font-awesome', 'https://kit.fontawesome.com/22223faeab.js', array(), '5.12.1', false );
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', JSPATH . 'app.js', array(), get_version_number(), true );
}
add_action( 'wp_enqueue_scripts', 'scripts' );

add_filter( 'style_loader_tag', 'add_fontawesome_attribute', 10, 2 );
function add_fontawesome_attribute($link, $handle) {
	if( $handle === 'font-awesome' ) {
		$link = str_replace( '/>', 'crossorigin="anonymous" />', $link );
	}
	return $link;
}

//CUSTOM POST TYPES
// add_action( 'init', 'codex_custom_init' );
// function codex_custom_init()
// {
//  //Leads
//  $labels = array(
//      'name' => _x('Users', 'post type general name'),
//      'singular_name' => _x('User', 'post type singular name'),
//      'add_new' => _x('Add User', 'New'),
//      'add_new_item' => __('Add New User'),
//      'edit_item' => __('Edit User'),
//      'new_item' => __('New User'),
//      'all_items' => __('All Users'),
//      'view_item' => __('View User'),
//      'search_items' => __('Search Users'),
//      'not_found' =>  __('User not found'),
//      'not_found_in_trash' => __('User not found in trash'),
//      'parent_item_colon' => '',
//      'menu_name' => 'Users'
//  );
//  $args = array(
//      'labels' => $labels,
//      'public' => false,
//      'publicly_queryable' => true,
//      'show_ui' => true,
//      'show_in_menu' => true,
//      'query_var' => true,
//      'rewrite' => true,
//      'capability_type' => 'post',
//      'has_archive' => false,
//      'hierarchical' => false,
//      'menu_position' => 4,
//      'menu_icon' => 'dashicons-businessman',
//      'supports' => array( 'title' )
//  );
//  register_post_type('user',$args);
// }

//Indent JSON
// function indent($json)
// {
//     $result      = '';
//     $pos         = 0;
//     $strLen      = strlen($json);
//     $indentStr   = '  ';
//     $newLine     = "\n";
//     $prevChar    = '';
//     $outOfQuotes = true;

//     for ($i=0; $i<=$strLen; $i++) {

		// Grab the next character in the string.
		// $char = substr($json, $i, 1);

		// Are we inside a quoted string?
		// if ($char == '"' && $prevChar != '\\') {
		//     $outOfQuotes = !$outOfQuotes;

		// If this character is the end of an element,
		// output a new line and indent the next line.
		// } else if(($char == '}' || $char == ']') && $outOfQuotes) {
		//     $result .= $newLine;
		//     $pos --;
		//     for ($j=0; $j<$pos; $j++) {
		//         $result .= $indentStr;
		//     }
		// }

		// Add the character to the result string.
		// $result .= $char;

		// If the last character was the beginning of an element,
		// output a new line and indent the next line.
//         if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
//             $result .= $newLine;
//             if ($char == '{' || $char == '[') {
//                 $pos ++;
//             }

//             for ($j = 0; $j < $pos; $j++) {
//                 $result .= $indentStr;
//             }
//         }

//         $prevChar = $char;
//     }

//     return $result;
// }

//Print JSON
// function printJSON($array)
// {
//  $json = json_encode($array);
//  header('Content-Type: application/json',true);
//  echo indent($json);
// }

?>